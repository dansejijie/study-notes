# 00

1 亿的 1 字节数据 是有 100MB 大小

# 01 为什么要学习数据结构和算法？

...

# 02 学哪些内容

- 数据结构
  数组、链表、栈、队列、散列表、二叉树、堆、图、True 树
- 算法
  递归、排序、二分查找、搜索、哈希算法、贪心算法、分治散发、回溯算法、动态规划、字符串匹配算法

# 03 时间复杂度 大 O 是如何计算的

# 04 最好、最坏、平均时间复杂度

# 05 为什么数组从 0 开始编号

- 一般是因为 C 开始就是这么设计的，所以后来语言都这么做了。
- 趣谈
  若初始为 0，找第 k 个内存地址：
  a[k] = base*address + k * type _ size
  若初始为 1，找第 k 个内存地址 对于 CPU 来说多了个<font color="red">减法运算</font>：
  a[k] = base_address + (k-1) _ type_size

# 06 链表实现 LRU 缓存

- 常用缓存策略

  - FIFO 先进先出策略
  - LFU （Leatest Frequently Used） 最少使用策略
  - LRU (Least Recently Used) 最新最少使用策略

- 双向链表对比单向链表的优势

  - 删除 （需要知道该节点的前后节点）
    - 已知值删除节点 无优势
    - 在已知节点情况下删除该节点，有优势

- <font color="red">链表实现 LRU 缓存思路</font>

  - 维护一个有序单链表， 越靠近唯独的是越早访问的，当一个心的数据被访问时，我们从连表头开始顺序遍历链表
  - 若此数据之前已经被缓存在链表中，我们便利得到对应的节点，并将其从原来位置删除，然后在插入到链表的头部
  - 若数据没有在缓存链表中，则
    - 若缓存未满，表的头部则此节点直接插入到链
    - 若缓存已满，则链表尾节点删除，将新数据节点插入链表的头部

- 如何判断是回文字符串的思考
  快慢指针

  ```
  /**
  * Definition for singly-linked list.
  * public class ListNode {
  *     int val;
  *     ListNode next;
  *     ListNode(int x) { val = x; }
  * }
  */
  class Solution {
    public boolean isPalindrome(ListNode head) {
      if (head == null || head.next == null) {
        return true;
      }

      ListNode prev = null;
      ListNode slow = head;
      ListNode fast = head;

      while (fast != null && fast.next != null) {
        fast = fast.next.next;
        ListNode next = slow.next;
        slow.next = prev;
        prev = slow;
        slow = next;
      }

      if (fast != null) {
        slow = slow.next;
      }

      while (slow != null) {
        if (slow.val != prev.val) {
          return false;
        }
        slow = slow.next;
        prev = prev.next;
      }

      return true;
    }
  }
  ```

# 07 链表的实现

    * 求链表的中间节点
      思路： 快慢指针
    * 求链表的倒数第n个节点
      思路：快慢指针，快指针每次步长为n
    * 检测链表是否有环
      思路：快慢指针，若能相遇则有环
    * 合并两个有序列表
      思路：遍历两个链表，比较两个链表节点的大小，修改当前节点指向较小的节点。

# 08 栈 浏览器实现前进后退 （入栈、出栈时间复杂度都为 O（1））

- 栈在括号匹配中的应用 判断 [{{}]{}] 是否是合法格式
  左到右依次扫描字符串，当扫描到做括号时，则将其压入栈，当扫描到右括号时，从斩定取出左括号，若能匹配，则继续扫描下一个字符串。

# 09 队列 线程池

- 线程池会用到队列排队请求

# 10 递归 如何实现三行代码找到最终推荐人

# 排序（上）：为什么插入排序比冒泡排序更受欢迎？

![avatar](images/WX20190219-103550.png)

- 空间复杂度为 O（1）的也叫原地排序算法，如冒泡排序，插入排序，选择排序
- <font color="red">稳定性指标</font>，指的是如果待排序的序列中存在值相等的元素，经过排序之后，相等元素之间原油的先后顺序不变。
  比如有一组数据 2，9，3，4，8，3。按照大小排序之后就是 2，3，3，4，8，9。3 与 3 之前顺序不变。
  <font color="red">冒泡排序是个稳定排序算法</font>

* 冒泡排序 原地排序算法、稳定排序算法，空间复杂度为 O（1），时间复杂度为 O（n^2）
  ```
  public void bubbleSort(int a[], int n) {
    if(n<=1) {
      return a;
    }
    for(int i = 0; i < n; i++) {
      boolean flag = false;
      for(int j = 0; j<n-i-1;++j) {
        if(a[j]>a[j+1]) {
          int temp = a[j];
          a[j] = a[j+1];
          a[j+1] = temp;
          flag = true
        }
      }
      if(!flag) {
        return a;
      }
    }
  }
  ```
* 插入排序 原地排序、稳定性排序、时间复杂度为 O（n）,空间复杂度为 O（n^2）
  ![avatar](images/WX20190219-130705.png)
  ```
  public void insertionSort(int a[], int n) {
    if(n<=1) {
      return a;
    }
    for(int i = 1;i<n;i++) {
      int value = a[i];
      int j=i-1;
      for(;j>-1;j--) {
        if(a[j]>value) {
          a[j+1]=a[j]
        } else {
          break;
        }
      }
      a[j+1] = value; // 插入元素
    }
  }
  ```

# 15 二分查找 如何用最省内存的方式实现快速查找功能？

- 必要条件
  - 有序
  - 数组
  - 不可太大 （内存爆满）

# 16 二分查找 如何快读定位 IP 地址

思路：ip 换整型，查找最后一个小于等于某个给定值的元素

- 查找第一个值等于给定值的元素
  ![avatar](images/WX20190211-183403.png)
- 查找最后一个值等于给定值的元素
  ![avatar](images/WX20190211-183412.png)
- 查找第一个值大于给定值的元素
  ![avatar](images/WX20190211-183417.png)
- 查找第一个值小于给定值的元素
  ![avatar](images/WX20190211-183424.png)

# 17 跳表：为什么 Redis 一定要用跳表来实现有序集合？

- Redis 的有序集合 Sorted Set 就是用跳表来实现，支持查找、插入、删除、<font color="red">按照区间查找数据</font>，红黑树在区间查找数据里表现并不好。所以用了跳表
- 二分查找是基于数组下标的可随机访问特性，那链表可以采用跳表来实现。<font color="red">建立索引的单链表</font>
  ![avatar](images/WX20190212-120627.png)

- 跳表的插入为了防止退化为单链表，所以需要一个随机函数来实现是否在每次插入时在索引层上建立索引
  ![avatar](images/WX20190212-120850.png)

- 跳表<font color="red">时间复杂度为 O（m\*logn）,空间复杂度为 O（n）</font>

# 18 word 文档中单词拼写检查功能是如何实现的

- 散列表 （如何解决散列冲突）

  - 开放寻址
  - 链表法

- 单词拼写检查功能解决办法
  常用英语 20 万个，平均长度 10 个字母，一个单词平均占 10 字节，也就总共占 2MB 内存，因此可以用散列表，单词首字母做 key。（其实可以用<font color="red">树</font>来实现）

- 思考 （<font color="red">重复数据多，通过散列表实现减少查重</font>）
  - 假设有 10 万跳 URL 访问日志，如何按照访问次数给 URL 排序
    思路：URL 做 key，次数做 value,存入散列表，时间复杂度 O（n），然后针对 value 做快速排序， 时间复杂度为 O（nlogn）
  - 有两个字符串数组，每个数组大约有 10 万条字符串，如何快速找出两个数组中相同的字符串
    思路：将一个字符串数组遍历组成散列表，然后另一个字符串数组遍历比对散列表找到相同字符串数组

# 23 二叉树基础（上）：什么样的二叉树适合用数组来存储？

- 概念理解
  ![avatar](images/WX20190213-103836.png)

* <font color="red">完全二叉树为何独特，且要求最后一层的字节点都靠左的原因</font>

  - 完全二叉树存在数组的方式。
    将根节点存储在下标 i=1 的位置，那么左子节点存储在下表 2*i=2 的位置，右子节点存储在 2*i+1=3 的位置，以此类推。
    ![avatar](images/WX20190213-111228.png)
  - 完全二叉树用数组存最省内存 ，无需存指针
  - 最后一层节点若靠右，则会浪费内存
    ![avatar](images/WX20190213-111310.png)

* 前中后序遍历 （<font color="red">前中后指的是本身节点的访问顺序</font>）

  - 前序遍历：先打印本身，在打印左子树，最后右子树
  - 中序遍历：先打印左子树，再打印本身，最后右子树
  - 后续遍历：先打印左子树，再打印右子树，最后节点本身
    ![avatar](images/WX20190213-112127.png)
    ![avatar](images/WX20190213-112351.png)

* 思考：
  - 给定数据 1、3、4、5、9、10 ，可以构建多少种不同的二叉树
    思路：如果是完全二叉树，老师说过可以放在数组里面，那么问题是否 可以简化为数组内的元素有多少种组合方式，这样的话，就是 n!

# 24 二叉树基础（下）：有了如此高效的散列表，为什么还需要二叉树？

- 二叉树的查找、插入、删除（删除分三种，其中有两个字节点的话，找到<font color="red">右子树最小节点替换为被删除节点</font>。）
  ![avatar](images/WX20190213-113822.png)

* 如何左一个支持重复数据的二叉查找树

  - 第一种：每个节点通过链表和支持动态扩容的数组等数据结构，吧值相同的数据都存储在同一个节点上。
  - 第二种：相同数据插入的时候插在相同节点的右子树，在查找的时候，若遇到相同节点，则继续在右子树查找，直到遇到叶子节点。

* 在有散列表的情况下，为啥还需要二叉树？
  - 散列表构造比二叉树要复杂，如散列函数的设计，冲突解决，扩容等。
  - 散列表数据是无需存储的。要输出有序数据的画，要先进行排序。二叉树进行终须排列，就可以输出有序集合。

# 25 红黑树（上）：为什么工程中都用红黑树这种二叉树？

- 平衡二叉树是为了解决普通额额茶树在频繁的插入、删除等动态更新节点的情况下，出现时间复杂度退化的问题
- 红黑树是不严格的平衡二叉树，一般会默认平衡二叉树就是红黑树

# 27 递归树：如何借助树来求解递归算法的时间复杂度？

# 28 | 堆和堆排序：为什么说堆排序没有快速排序快？

- 堆就是完全二叉树存在数组里
- 堆的叶子节点的下标开始在 n/2+1
- 堆的插入和删除的时间复杂度都为 O(logn)
- 堆排序
  - 。。。
- 堆排序里元素会不断的被重复构建堆，所以堆排序的交换次数大于快速排序

# 29 | 堆的应用：如何快速获取到 Top 10 最热门的搜索关键词？

- 堆的应用场景 - 优先队列 （java PriorityQueue， 堆顶为优先级最高）

  - 优先队列

    - 合并有序小文件
      假设杨浦 100 个文件，每个文件大小是 100MB，每个文件中存存储的都是有序的字符串，希望将 100 个小文件合并成有序的大文件。
      思考 0、由于总体大小为 10G 左右的文件，<font color="red">因此不能做全排序</font>。
      思考 1、取 100 个文件的首字母，放在数组里排序，找到最小的字符串放入大文件里，然后找到对应的文件，放入下个字符串到数组里，最终完成合并。由于这样的花在数组查找里时间复杂度为 O(n=100)
      思考 2、取 100 个文件的首字母，放在堆里排序，找到最小的字符串放入大文件里，然后找到对应的文件，放入下个字符串到堆里，最终完成合并。由于这样的<font color="red">花在数组查找里时间复杂度为 O(logn=100)</font>

    * 高性能定时器
      有一个定时器，维护很多任务，每个任务将在不定时间内需要执行，不能每次隔秒扫一遍队列，太耗性能了。因此可以通过优先队列实现。（<font color="red">为啥不能直接用有序数组呢？因为在涉及插入删除时优先队列时间复杂度为 O（n）</font>）

  * Top K

    - 维护一个大小为 K 的小顶堆即刻

  * 中位数 （衍生 求其他百分位的数据）(如何快速求解 99%的接口响应时间)

    - 维护一个大顶堆和一个小顶堆，小顶堆数据都大于大顶堆中的数据, 两个堆数据量相同

    * 每次插入数据时，堆化后，将堆中一数据插入到另一个堆中保持数据量平衡。

    * 由于堆化时间复杂度为 O（logn）,插入为 O（1），因此整体时间复杂度为 O（logn）

# 30 | 图的表示：如何存储微博、微信等社交网络中的好友关系？

    * 邻接矩阵
      ![avatar](images/WX20190225-101629.png)
    * 邻接表
      ![avatar](images/WX20190225-101812.png)

# 31 | 深度和广度优先搜索：如何找出社交网络中的三度好友关系？

- 六度分割理论
- 广度优先搜索 时间复杂度 O（E），空间复杂度 O（V） V：顶点个数，E：边个数
  ![avatar](images/WX20190225-102644.png)

* 深度优先搜索
  广度优先搜索是队列思想
  深度优先搜索是栈思想
  ![avatar](images/WX20190225-105340.png)

# 32 | 字符串匹配基础（上）：如何借助哈希算法实现高效字符串匹配？

- 字符串匹配算法很多，目前介绍 BF 算法、RK 算法、BM 算法、KMP 算法
- 字符串匹配 即在 n 长的字符串里找一个 m 长的特定字符串。
- BF 算法 即暴力算法，直接在每次后移一位截取 m 长字符串，进行 m 个字符比较
  ![avatar](images/WX20190225-110141.png)
  时间复杂度：O（n\*m）,n 为后移次数，m 为字符比较次数
  空间复杂度：O（n）
- RK 算法 针对 BF 算法的优化，每次 m 长字符串比较浪费时间，因此将 n-m+1 个长度为 m 的<font color="red">子串进行哈希运算求的哈希值</font>，进行数值比较。
  关键是哈希运算。因为求解哈希值本身是要遍历 n-m+1 个长度为 m 的子串
  哈希运算设计：
  将要匹配的字符串的字符集中只包含 K 个字符，则用 K 进制来表示一个子串，如 "isodfho" 包含 6 个字符，则用 6 进制表示，该字符串的值是：
  i*6^8 + s*6^7 + ....
  此公式可以看出<font color="red">第 j 个子串于 j-1 个子串是有求解关系</font>： h[j] = h[i-1]-k^(m-1) \* ....
  因此求解各个子串是时间复杂度为 O（n），总共比较 n-m + 1 个子串的哈希值，时间复杂度为 O（n）。

# 33 | 字符串匹配基础（中）：如何实现文本编辑器中的查找功能？

BM 算法 主要针对 m 长字符串在不匹配时，<font color="red">往后移动多少位的问题</font>。具体细节暂理解。涉及到坏字符规则和好后缀规则。
时间复杂度为 O（n/m）

KMP 算法，最知名的算法。

# 35 | Trie 树：如何实现搜索引擎的搜索关键词提示功能？

- Trie 适合通过前缀来匹配查找对应的字符串集
- 应用场景：<font color="red">输入法自动补全、搜索关键词提示功能</font>
- 时间复杂度为 O（K），K 为字符串长度，空间复杂度为 O（M\*（1-2^K）/(1-2)） ,等比数列求和，M 为字符集数量，K 为最初字符串的长度。
- Trie 是个空间换时间的，很多时候需要<font color="red">考虑内存够不够用</font>
- Trie 是个多叉树，每个节点存储的是一个数组，数组通过字符的与 a 的 <font color="red">assic 码相减</font>得到的下标
  ![avatar](images/WX20190226-111412@2x.png)
  ![avatar](images/WX20190226-111422@2x.png)

# 37 | 贪心算法：如何用贪心算法实现 Huffman 压缩编码？

- Huffman（霍夫曼）压缩编码，<font color="red">不等长编码</font>。原理就是 6 个字符每个字符战 8bit，转成 3 进制（可表示 8 种类型可涵盖），各占 3bit，进一步处理，对高频字符的编码短点编码，低频字符长点编码，<font color="red">后一个编码的前缀不会与前一个编码的后缀相同</font>

* 原理步骤：
  ![avatar](images/WX20190226-114754@2x.png)
  ![avatar](images/WX20190226-114805@2x.png)

* 思考题
  1、在一个非负整数中，希望溢出 K 个数字，让剩下的数字值最小，如何选择移除哪几个 k 数字，例如 502，移除 2 个，那移除 5、2，剩 0 为最小
  思路：首先在前 k 个数字里移除最大的数字，然后在剩下的前 K 个数字里移除最大的

# 38 | 分治算法：谈一谈大规模计算框架 MapReduce 中的分治思想

# 43 | 拓扑排序：如何确定代码源文件的编译依赖关系？

- 所谓拓扑排序就是有些对象具有依赖关系，在排序的时候顺序不变
- 拓扑排序是基于有向无环图的一个算法

# 45 | 位图：如何实现网页爬虫中的 URL 去重功能？

- 假设有 10 亿个网页需要爬虫（网页里面有很多链接，高概率很多是重复的），如何防止重复爬虫？
  - 思考：一字节 10 亿个平均需要 1G，链接假设有 60 字节的话，则有至少 60G 的内存要存
  - 思路一、采用散列表，需要链接来支持解决冲突的散列表，因此所耗内存可能到 100G。难以有这样的机器
  - 思路二、分治，根据链接前缀字母假设有 a-z,26 个，分成 26 份散列表，这样每台机器只需 5G 左右的内存。
  - 思路三、位图、布隆过滤器。<font color="red">（降低内存消耗）</font>
    - 布隆过滤器： 有多个哈希函数，允许一定量误差。

* 假设有 1-10 亿范围之间的 1 千万个整数，如何判断某个整数在这其中

# 46 | 概率统计：如何利用朴素贝叶斯算法过滤垃圾短信？

# 47 | 向量空间：如何实现一个简单的音乐推荐系统？

- 方法一：找到跟你口味偏好相似的用户，把她们爱听的歌曲推荐给你。
  - 数字大小表示喜爱程度
  - 用户之间的相似度用欧几里得来求解
    ![avatar](images/WX20190228-151950.png)
    ![avatar](images/WX20190228-151956.png)
- 方法二：找到跟你喜爱的歌曲特征相似的歌曲，把这些歌曲推荐给你。
  ![avatar](images/WX20190228-152005.png)

# 48 | B+树：MySQL 数据库索引是如何实现的？

问题：
1、根据某个值查找数据，比如 select _ from user where id = 1234;
2、根据区间值来查找某些数据，比如 select _ from user where id > 1234 and id < 2345;

    思考（如何实现索引用什么数据结构）：
      1、二叉树：区间查找耗时，不建议
      2、散列表：不支持区间查找
      3、跳表：可以实现

- 这里采用 B+树来实现 <font color="red">（m 叉树）</font>

  - 结构
    - 非叶子节点<font color="red">不存储数据本身，只是作为索引</font>
    * 叶子结点串联，双向链表
      ![avatar](images/WX20190228-155548.png)
  - m 叉树
    - 由于构建节点非常耗内存，因此采用多叉树
    - 由于不管内存中数据还是磁盘中数据，操作系统都是按页（一页大小为 4KB）来读取，若数据量大于一页大小，会出发多次 IO 操作。
      因此再选择 M（<font color="red">如何得出 M</font>？一个节点里包含了多个字节点引用，引用的综合小于 m） 大小的时候，尽量让每个节点的大小等于一个页的大小。读取一个节点只需一次磁盘操作
      ![avatar](images/WX20190228-160238.png)

  * 插入导致的节点分裂
    - 节点插入导致大于 M 数量，因此由下往上分裂
      ![avatar](images/WX20190228-160511.png)
  * 删除导致节点的合并

    - 定义一个阀值，小于 m/2 的时候的时候才合并相邻子节点 <font color="red">（那刚开始构建时可以考虑少于 m 的叉树，避免插入频繁分裂）</font>
    - 合并节点后会导致数量大于 m，因此可以考虑节点分裂。

  * 相比 B+树，还有 B-树、B 树
    B-树就是 就是 B-Tree，实际就是 B 树
    <font color="red">B 树时低版的 B+树</font>
    B 树的节点存储数据
    B 树的叶子结点不需要链表来串联

  * B+树相比跳表的优点
    - 树的高度低
    - 考虑的分页的问题

# 49 | 搜索：如何用 A\*搜索算法实现游戏中的寻路功能？

- 最短路径优化改进，非最优路径，而只是一个优解
- 改进 1:采用优先队列，每次出队列的是顶点到起点的最短路径

* 改进 2:增加一个因素，顶底到重点的曼哈顿距离(Math.abs(v1.x-v2.x)+Math.abs(v1.y-v2.y))，相对欧几里得距离，少了乘法开平方。
  综合根据 顶点到起点的距离+曼哈顿距离 来决定出队列顺序。
  ![avatar](images/WX20190305-112741.png)

# 50 | 索引：如何在海量数据中快速查找某个数据？

- 无

# 51 | 并行算法：如何利用并行处理提高算法的执行效率？

- 并行 = 多线程处理

* 并行排序
  - 将数据集分成 n 份，对 n 份多线程进行排序，然后合并后再排序
  - 将数据集获取区间范围，分成 n 份区间，再将数据分配到对应区间，排序后可以直接合并

- 并行查找
  - 将数据集分成 n 份散列表

* 字符串匹配
  - 将超大文本分成 n 份进行匹配

# 52 | 算法实战（一）：剖析 Redis 常用数据类型对应的数据结构

- Redis 是一个内存数据库
- Redis 常用数据类型有字符串、列表、字典、集合、有序集合
- 列表

  - 当数据值小、数据量小时（个数不超过 512），会采用压缩列表 （<font color="red">数组实现，每段数据大小不等长，不支持随机访问</font>）
    ![avatar](images/WX20190304-175821.png)
  - 当数据值大或数据量大的时，会采用<font color="red">循环列表</font>

* 字典

  - 当数据量小（个数不超过 512）时，会采用压缩列表
  - 当数据值大或数据量大的时，会采用散列表，对于哈希冲突，采用链表法来解决

* 集合

  - 数据量小、个数不超过 512 个时 采用<font color="red">有序集合（跳表）</font>
  - 数量量大用<font color="red">散列表</font>

*

# 53 | 算法实战（二）：剖析搜索引擎背后的经典数据结构和算法

- 主要涉及 <font color="red">搜集、分析、索引、查询 </font>四个关键步骤

  - 搜集
    - 文件做队列（支持断电续爬）
    - 网页判重（使用布隆过滤器，半消失存本地，防止机器宕机重启）
    - 原始网页存储（存储到文件 ，每个网页通过标识符分割（有网页 id），当文件过大后，新建文件存储，比如 1g）
      ![avatar](images/WX20190304-172859.png)
    * <font color="red">doc_id.bin</font> 维护一个文件，文件内容包含了网页 id 和网页链接，

  * 分析
    - 剔除无关信息，如 JavaScript, CSS 等，可以采用 AC 自动机多模式匹配算法。只保留用户可见的文本内容，
    * 分词创建临时索引(采用 trie 树实现最长匹配规则)
      得到单词列表后，将单词（单词编号）与网页（网页编号）的对应关系写入到临时索引文件中
      ![avatar](images/WX20190304-175034.png)
      <font color="red">term_id.bin</font> 单词编号的生成通过维护的计数器+散列表（用于检验是否生成过单词编号）来实现
  * 索引

    - <font color="red">index.bin</font> 通过临时索引建立倒排序索引 （由于临时索引文件太大，无法使用内存排序，因此采用多路归并排序）
      ![avatar](images/WX20190304-175604.png)
    - <font color="red">term_offsert.bin</font> 建立索引偏移文件，方便知道索引偏移位置
      ![avatar](images/WX20190304-175604.png)

  * <font color="red">查询</font>
    - 除 index.bin 文件比较大外，其他文件比较小，可以存入内存中
    - 用户输入字符串，分词得到多个单词
    - 在 term_id 里查找多个单词的 id
    - 在 term_offsert.bin 里查找多个单词的偏移量
    - 在 index_id 里查找到多个网页链接 id
    - 根据网页 id 在 doc_id.bin 里找到对应的链接。

* 优化
  - 计算爬虫时网页权重
  - 计算查询网页结构排名

# 54 | 算法实战（三）：剖析高性能队列 Disruptor 背后的数据结构和算法

- Dosriptor 时一个内存消息队列，在线程之间用于消息传递的队列，在很多知名项目中有广泛应用

* 比 Javad 的内存消息队列的 ArrayBlockingQueue 性能还要高一个数量级，可以算得上最快的内存消息队列
* 队列一般可分为无界队列（链表实现），有界队列（数组实现），有界队列应用场景更加广泛。无界队列容易引发 OOM
* 由于有界队列在涉及添加、删除时会涉及到数据的搬迁操作，导致性能遍查哈，因此大部分顺序队列选择的是循环队列
* 并发队列 <font color="red">并发</font>两字意思是线程安全的
* 一般保证线程安全是在写入的时候<font color="red">加锁</font>，读取的时候也加锁
* Dosriptor 在循环队列里如何保证同时有多个线程写入安全或读取安全的：
  - 其中一个写入数据线程向循环队列里<font color="red">申请连续</font>的内存空间，这时候该内存空间为该线程独有
  * 其中一个读取线程向循环队列里<font color="red">申请连续</font>的内存空间，这时候内存空间为该线程独有
    缺陷：
    当写入数据申请连续空间后，在未写完数据之前，<font color="red">无法申请该空间后续的空间读取</font>

# 55 | 算法实战（四）：剖析微服务接口鉴权限流背后的数据结构和算法

- 微服务
  优点：
  - 有利于<font color="red">团队组织架构</font>、每个应用可以<font color="red">独立</font>上线维护
    缺点：
  - 服务之间<font color="red">调用关系变得更复杂</font>，出错的概率、debug 的单独高了很多

* 服务技术之鉴权（接口访问权限验证）

  - 精确匹配规则
    - 根据字符串的大小给规则排序，采用二分查找算法
      ![avatar](images/WX20190301-104645.png)
  - 前缀匹配规则
    - 采用 Trie 树， 每个节点存储的是单个字符串
      ![avatar](images/WX20190301-104651.png)
      ![avatar](images/WX20190301-104659.png)
  - 模糊匹配规则 （通配符的匹配）
    - 回溯算法
      ![avatar](images/WX20190301-104705.png)

* 服务技术之限流（接口访问单位时间不得超过阀值）
  - 构建一个 K+1 的循环队列，K 为阀值大小
  - 每当新接口请求时，验证与该新接口请求时间超过 1s 的在队列里的接口，若超过单位时间，则删除
  - 验证队列里有无空余位置，若有，则允许请求，无则拒绝该请求

# 并发包 （线程安全的数据结构）（java.util.concurrent）

# Atomic （原子操作）

一词跟原子有点关系，后者曾被人认为是最小物质的单位。计算机中的 Atomic 是指不能分割成若干部分的意思。如果一段代码被认为是 Atomic，则表示这段代码在执行过程中，是不能被中断的。通常来说，原子指令由硬件提供，供软件来实现原子方法<font color="red">（某个线程进入该方法后，就不会被中断，直到其执行完成）</font>

# Queue 队列

## BlockingQueue （阻塞队列）(BlockingDeque 双端队列)

- ArrayBlockingQueue
  由数组结构组成的有界阻塞队列
- LinkedBlockingQueue
  由链表结构组成的有界阻塞队列
- PriorityBlockingQueue
  支持优先级排序的<font color="red">无界</font>阻塞队列

# CopyOnWriteArrayList

针对 ArrayList 的 线程安全

# CopyOnWriteArraySet

针对 Set 的线程安全

# ConcurrentMap

这对 Map 的线程安全

# ConcurrentHashMap

针对 HashMap 的线程安全

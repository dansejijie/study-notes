# 响应式
  * 采用ES5的Object.defineProperty 实现getter/setter方法。因此IE8及低版本不支持Vue
  * 针对Object和Array无法支持响应式，可以用另一种方法实现
    * Object
            Object.assign(this.obj, {a: 1, b: 2})
            => Object.assign({}, this.obj, {a: 1, b: 2})
    * Array
            this.arr[2] = ‘a’;
            => this.arr.splice(2, 1, ‘a’);
  * $nextTick() 返回一个 Promise
  
# Mixin & extends
  * 递归合并
    * 钩子函数会合并为数组，如 created、destoryed
    * 可以实现自定义混入

# 自定义指令实现
  * 如 v-focus ,

# 过渡动画 transition
  * 自动嗅探目标元素是否应用了 CSS 过渡或动画，如果是，在恰当的时机添加/删除 CSS 类名。 （辅助提供的）
  ![avatar](images/WX20200602-100418@2x.png)
  如果你使用了 <transition name="my-transition">，那么 v-enter 会替换为 my-transition-enter。

# 深入组件
* 组件注册之名字驼峰会转换为 kebab-case 
* 自定义事件
  ![avatar](images/WX20200604-094240@2x.png)
* 一个组件上的 v-model 默认会利用名为 value 的 prop 和名为 input 的事件
* keep-alive 
  ```
  <!-- 失活的组件将会被缓存！-->
  <keep-alive>
    <component v-bind:is="currentTabComponent"></component>
  </keep-alive>
  ```

# 生命周期
  共8个，beforeCreate, created, beforeMount, mounted, beforeUpdate, updated, boforeDestory destoryed

# 边界情况
都是不建议的操作
  * this.$root 这样自组件就可以访问根组件的属性
  * this.$parent 子组件可以访问父组件的属性
  * this.$refs 只在 组件渲染之后生效，应该避免在模版或计算属性里面生效
  * provide 和 reject 实现 后代属性共享
    * 祖先组件不需要知道哪些后代组件使用它提供的属性  
    * 后代组件不需要知道被注入的属性来自哪里
    * 然而，依赖注入还是有负面影响的。它将你应用程序中的组件与它们当前的组织方式耦合起来，使重构变得更加困难。同时所提供的属性是非响应式的。这是出于设计的考虑，因为使用它们来创建一个中心化规模化的数据跟使用 $root做这件事都是不够好的。如果你想要共享的这个属性是你的应用特有的，而不是通用化的，或者如果你想在祖先组件中更新所提供的数据，那么这意味着你可能需要换用一个像 Vuex 这样真正的状态管理方案了。
  * this.$forceUpdate 强制更新
  * v-once 有大量静态内容时，可以保持一次更新，然后缓存。
注意情况
  * 自身循环引用。可以循环，但注意边界条件
  * 两组件循环引用。
    ![avatar](images/20200605135955.jpg)
    ![avatar](images/20200605135839.jpg)


# vue.cli

# router
  * vue 的 router.push 本质采用了history.pushState()，会按需刷新页面, location.href = '/xx' 会刷新页面。

# vue 3.0 Proxy
  * https://www.jianshu.com/p/2a8ec76e0090
  * ES6 语法，自定义化任何对象（数组、函数、对象）的原本方法，如 set、get、has、defineProperty 等。
  * 补齐 Vue 2.0 之前 Object.definedProperty 方法的缺陷，如无法监听 属性的添加和删除、数组索引和长度的变更，并可以支持 Map、Set、WeakMap 和 WeakSet

  ![avatar](images/WX20200720-155614@2x.png)

## Proxy 功能理解
  * get: 实现访问的log

  ![avatar](images/WX20200720-155747@2x.png)

  * set: 实现赋值的校验

  ![avatar](images/WX20200720-155841@2x@2x.png)

  * has: 隐藏私有属性的访问 _xxx

  ![avatar](images/WX20200720-160027@2x.png)




# 简单双向数据绑定实现方式

 ![avatar](images/WX20200720-160206@2x.png)
 ![avatar](images/WX20200720-160229.png)






    




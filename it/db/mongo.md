# mongo 安装

## linux 安装
  * 下载 wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-rhel62-3.4.2.tgz
  * 解压 tar -zxvf mongodb-linux-x86_64-rhel62-3.4.2.tgz
  * 改名 mv mongodb-linux-x86_64-rhel62-3.4.2 mongodb
  * 新建数据库文件夹 mkdir -p /data/db
  * 新建数据库日志 touch /data/db/mongo.log
  * 后台运行服务 mongodb/bin/mongod -logpath /data/db/mongo.log -fork
    或 /usr/local/mongodb/bin/mongod
  * 安装位置 /usr/local/mongodb/bin

## mac 安装
  * 安装 sudo brew install mongodb
  * 安装位置 /usr/local/bin
  * 查看安装位置 which mongo  // /usr/local/bin/mongo
  * 运行服务 brew services start mongodb
    或 /usr/local/bin/mongod
  * 数据库目录 /data/db
  

# 说明
  * mongod 是用来连接到mongodb数据库服务器的，即服务端
  * mongo 是用来启动 mongodb shell 的,是 mongodb的命令行客户端

# mongo shell运行操作
  * 打开mongodb命令行 /usr/local/bin/mongo
  * 查看所有数据库 show dbs // admin、aypy、..
  * 切换数据库 use aypy
  * 查看所有集合 show collections // depthCrossMarket、collections2
  * 查询数据 db.depthCrossMarket.find().pretty()  // pretty(): 方便用户阅读数据,      find()内条件就自行查询



# 连接阿里云 mongodb shell 操作 (未实现 ，连不上)
  * 环境说明
    1. 本地已有 mongdb，并被启动
    2. 阿里云里有 mongodb ,并被启动，ip:47.110.44.208 port:27018 用户名:user 密码: password
    3. 要求连接到 阿里云的 mongodb,并查看数据库信息

  * 关闭客户端 mongodb 服务 
    brew services start mongodb
  * 查看更改ip 端口号等参数 /usr/local/bin/mongod -h   // --bind_ip,--port 
  * 连接云 mongodb
    /usr/local/bin/mongod --bind-ip 






# 创建 管理员 root 普通用户

## 创建 admin
  角色：userAdminAnyDatabase  （这是一个账号管理员的角色）
  admin用户用于管理账号，不能进行关闭数据库等操作，目标数据库是admin

  use admin
  db.createUser({user: "admin",pwd: "123456",roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]})


## 创建root
  创建完admin管理员，创建一个 超级管理员 root   角色：root
  root角色用于 关闭数据库 db.shutdownServer()

  db.createUser({user: "root",pwd: "123456",roles: [ { role: "root", db: "admin" } ]})

## 创建用户自己的数据库的角色
  当账号管理员和超级管理员，可以为自己的数据库创建用户了
（坑）这时候一定，一定要切换到所在数据库上去创建用户，不然创建的用户还是属于admin。

  use position
  db.createUser({user: "position",pwd: "123456",roles: [ { role: "dbOwner", db: "position" } ]})

## 查看用户
  创建完或者删完用户，可以查看当前的用户列表
  db.system.users.find()
  或者
  show users


## 删除用户
  删除用户必须由账号管理员来删，所以，切换到admin角色
  use admin
  db.auth("admin","123456")

  删除单个用户
  db.system.users.remove({user:"XXXXXX"})
  删除所有用户
  db.system.users.remove({})


# xcode
  * xcode7 发布后，个人Apple id 也能生成证书进行真机调试
    xcode 创建个人账户
    ![avatar](images/WX20190124-171655.png)

  * 新项目里设置个人 apple id
    注意，在创建证书的时候，需要开启全局VPN,不然无法联网创建
    ![avatar](images/WX20190124-171934.png)

  * 运行 react-native run-ios
    这时候会一直卡在 install third party 里，是因为要翻墙安装一个非常大的boost 的包，而它在下载的时候并没有走vpn过，这时候可以安装一个 Proxifier来设置所有请求走VPN。

  * 继续编译报 CFBundleIdentifier 错误。
  

  
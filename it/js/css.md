# css

* clientHeight、offsetHeight、scrollHeight、offsetTop、scrollTop
  * https://blog.csdn.net/qq_35430000/article/details/80277587

* word-wrap word-break white-space
  * https://www.cnblogs.com/dfyg-xiaoxiao/p/9640422.html
  
* sticky
  * https://blog.csdn.net/qq_35585701/article/details/81040901

  介于 position: relative 和 position: fixed 之间，相对父容器的固定位置。

  使用条件：
  1. 父元素不能overflow:hidden或者overflow:auto属性。
  2. 必须指定top、bottom、left、right4个值之一，否则只会处于相对定位
  3. 父元素的高度不能低于sticky元素的高度
  4. sticky元素仅在其父元素内生效
  5. sticky元素必须有高度

* css 样式优先级
  内联样式（在 HTML 元素内部） ->  内部样式表（位于 <head> 标签内部）-> 外部样式表 -> 浏览器缺省设置

* css 基本规则

  ```
  p {
    text-align: center;
    color: black;
    font-family: arial;
  }
  ```

* 继承
  <font color="red">// 有些浏览器不支持继承，因此需要使用 组选择器, 对子元素设置属性会避免被继承 </font>

  ```
  body  { 
    font-family: Verdana, sans-serif;
  }
  p, td, ul, ol, li, dl, dt, dd  {
    font-family: Verdana, sans-serif;
  }


* 派生选择器

  ```
  li strong {
    font-style: italic;
    font-weight: normal;
  }

  <li><strong>我是斜体字。这是因为 strong 元素位于 li 元素内。</strong></li>
  ```


* 类选择器、id 选择器

  ```

  td.fancy {
    color: #f60;
    background: #666;
	}

  #sidebar p {
	font-style: italic;
	text-align: right;
	margin-top: 0.5em;
	}
  
  ```


* 属性选择器

```
input[type="button"]
{
  width:120px;
  margin-left:35px;
  display:block;
  font-family: Verdana, Arial;
}
```

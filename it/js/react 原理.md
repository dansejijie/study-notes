# react 原理

react 是基于<font color="red">组件化开发的</font>，不是一个完整的 MVC 框架，更像是一个 MVC 里的 View 层。
组件之间一般由 props 和 state 来控制，一般官方是推荐在写自定义组件的时候采用受控组件的写法，组件有生命周期，每次创建、销毁、更新都会调用相应的方法。
react 针对频繁操作 Dom 导致的渲染瓶颈， 引入了<font color="red">虚拟 DOM</font>，避免了频繁操作真实的 DOM。
在写组件时，采用了<font color="red">JSX</font>写法。方便阅读，最终会被编译为 React.createClass

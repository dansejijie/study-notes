# 基础

## 变量和类型

- 7 种语言类型
  null, undefiend, string, number, function, symbol, boolean

- 四种底层属性及方法

  - 四属性：writable, enumerabel, value, configurable (能否使用 delete、能否需改属性特性、或能否修改访问器属性、，false 为不可重新定义，默认值为 true)
  - 两访问器属性：getter, setter

- 基本类型对应的内置对象，以及他们之间的<font color="red">装箱拆箱操作</font>

  - 如 string -> String
  - 每当读取一个基本类型的时候，后台就会创建一个对应的基本包装类型对象，从而让我们能够调用一些方法来操作这些数据。
    如 let a = "abc"; a.indexOf('a')

- 出现小数精度丢失的原因，JavaScript 可以存储的最大数字、最大安全数字，JavaScript 处理大数字的方法、避免精度丢失的方法

  - JS 采用双精度存储，即 64 位

    ![avatar](images/20190527110755.jpg)

    - 1 位表示符号
    - 11 位表示指数
    - 52 位表示指数

  - 最大安全数字 Math.pow(2, 53)

- 至少可以说出三种判断 JavaScript 数据类型的方式，以及他们的优缺点，如何准确的判断数组类型

  ![avatar](images/20190527112329.jpg)

  ![avatar](images/20190527112407.jpg)

  ![avatar](images/20190527112612.jpg)

  ![avatar](images/20190527112658.jpg)

## this

上下文环境，用于

```
var obj = {
  foo: function () { console.log(this.bar) },
  bar: 1
};

var foo = obj.foo;
var bar = 2;

obj.foo() // 1
foo() // 2
```

Javascript 是一个文本作用域的语言, 就是说, 一个变量的作用域, 在写这个变量的时候确定. this 关键字是为了在 JS 中加入动态作用域而做的努力. 所谓动态作用域, 就是说变量的作用范围, 是根据函数调用的位置而定的. 从这个角度来理解 this, 就简单的多.

this 是 JS 中的动态作用域机制, 具体来说有四种, 优先级有低到高分别如下:

1. 默认的 this 绑定, 就是说 在一个函数中使用了 this, 但是没有为 this 绑定对象. 这种情况下, 非严格默认, this 就是全局变量 Node 环境中的 global, 浏览器环境中的 window.
2. 隐式绑定: 使用 obj.foo() 这样的语法来调用函数的时候, 函数 foo 中的 this 绑定到 obj 对象.
3. 显示绑定: foo.call(obj, ...), foo.apply(obj,[...]), foo.bind(obj,...)
4. 构造绑定: new foo() , 这种情况, 无论 foo 是否做了绑定, 都要创建一个新的对象, 然后 foo 中的 this 引用这个对象.

## 原型和原型链

## 函数（特殊对象）function func(){}

- 每个函数都有一个 prototype 属性,该属性是一个指针，指向一个对象，该对象称之为原型对象，
  原型对象默认有一个属性 constructor，该属性也是一个指针，指向其相关联的构造函数（非指函数内部的 constructor）

  ```
    // 原型对象
    func.propotype = {
      constructor: xxx, // 默认指针，指向构造函数
      name: 'kitty',
      sayName: function() {
        console.log('kitty')
      }
    }

    func.prototype.constructor // f func(){}

  ```

* new 一个函数后，生成一个  实例（类型为 obejct），实例默认有 **proto** 属性和 constructor 属性，两个都是指针
  **proto** 属性指向 func.prototype
  constructor 属性指向 func

  ```
  let b = new func()
  typeof b === 'object'
  b.__proto__ === func.protptype  // true
  b.constructor === func // true
  ```

## 基本技术实现

- Promise 实现 见另一页
  (return new Promise((resolve, reject)=>{})).then(())
  * 一个class
  * 构建函数接受两个回调方法
  * 返回自身
  * 可多次被.then方法调用

* instanceOf 实现
  ```
  myInstanceOf(left, right) {
    const prototype = right.prototype
    let left = left.__proto__;
    while (true) {
      if(left == null) {
        return false;
      } else if(left == prototype) {
        return true;
      }
      left = left.__proto__
    }
  }
  ```

* 抖动和节流
  ![avatar](images/WX20200623-102408@2x.png)

  ![avatar](images/WX20200623-102420@2x.png)

  ![avatar](images/WX20200623-102427@2x.png)

## 技术知识

- 柯里化 见另一页

# 跨域
* https://segmentfault.com/a/1190000011145364?utm_source=tag-newest
* 跨域指的是浏览器，服务器不存在跨域说法。
## 跨域由来
* 浏览器认为非同源访问下，该访问总是会试图攻击攻击用户。因此产生同源策略来限制这个问题
* 同源：协议+域名+端口 三者相同。 http://wwww.baidu.com:8080/

## 跨域类型 
* 跨域访问分两种：网页访问非同源网页+网页访问非同源的服务器。

## 解决策略

### 网页访问非同源网页
* postMessage 
* location.name + iframe
  * 原理: location.name 在不同窗口为同一个值，可以通过location.name 来定义协议通信

### 网页访问非同源的服务器
* <font color="red">跨域资源共享（CROS）（通用解决办法）</font>
  * 情景：访问自己服务器，但服务器由多个，域名不同
  * 服务器在请求头添加 Access-Control-Allow-Origin: * 即可。告知服务器允许允许访问

* <font color="red">nginx代理跨域</font>
  * 情景：访问非自己服务器资源
  * 中间部署自己的代理服务nginx，由nginx在访问里追加请求头Access-Control-Allow-Origin: *，再转发给目标服务器

* Nodejs 中间件跨域
  * 原理同nginx代理跨域。只不过开启服务用的是node

* WebSocket 协议跨域
  * 浏览器 支持 WebSocket 跨域。

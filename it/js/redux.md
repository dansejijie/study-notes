# Redux

- 全局<font color="red">唯一一个 Store</font>, 由 <font color="green">createStore(reduce, initState, middleware)</font> 生成，内部包含：

  - getState() 返回 currentState
  - dispatch(action) 发送修改信息
  - <font color="red">currentState (保存数据)</font> (在 dispatch 传给自定义 reduce)
  - subscribe 生成 nextState 时，调用数组里注册的 subscribe

* <font color="green">combineReducers</font>，将多个自定义的 reduce 组装在一个 map 里，并<font color="red">返回一个主 reduce </font>函数 （combination）。

  - combination 主要作用是 在 dispatch 调用时，被当作 reduce 来调用，内部是调用 map 里的所有 自定义 reduce，并组合所有新的或旧的 state ，合并后返回给外面。

  ```
  const nextState = {};
  for (let i = 0; i < finalReducerKeys.length; i++) {
      const reducer = finalReducers[key]
      const previousStateForKey = state[key]
      const nextStateForKey = reducer(previousStateForKey, action)
      nextState[key] = nextStateForKey
    }
  ```

* <font color="green">applyMiddleware</font> 将多个 middleware 包装 dispatch, 使其<font color="red">可以链式</font>调用多个 middleware，调用链中最后一个 middleware 会接受真实的 store 的 dispatch 方法作为 next 参数，并借此结束调用链。所以，middleware 的函数签名是 <font color="red">({ getState, dispatch }) => next => action。</font>

- react-redux

  - provider

    - 继承 Component ，接受一个 store ，并且注册订阅 <font color="red">subscribe</font> ，若有更新并调用 setState

    * store 放在 <font color="red">context</font>里传给子组建

  - connect
    - 获取 context 里的 store， 并且注册订阅 <font color="red">subscribe</font> ，若有更新，根据 mapStateToProps 来选择对应的数据。
    * mapStateToProps
    * mapDispatchToProps
      定义了 UI 组件通信给 Store 的方法

* redux-saga

  - redux-saga 中间件，解决复杂的异步
  - redux-saga 使用了 Generator,可以使用同步来写代码，相比 redux-thunk 可以更会出现回调地狱，可以做一些 async 函数做不到的事情 (无阻塞并发、取消请求)

  * saga 可以被看作是后台的允许进程，监听属于自己的 action。

* dva
  - 装了 redux，saga,减少很多重复代码比如 action reducers 常量等
  * 简单，通过 reducers, effects 和 subscriptions 组织 model
  * 插件机制，比如 dva-loading 可以自动处理 loading 状态，不用一遍遍地写 showLoading 和 hideLoading
  * 支持 HMR，基于 babel-plugin-dva-hmr 实现 components、routes 和 models 的 HMR
  * 增加了一个 Subscriptions, 用于收集其他来源的 action, eg: 键盘操作

- create

Redux 是如何将 state 注入到 React 组件上的？

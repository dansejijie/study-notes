# 概览
  * CommonJS,AMD,CMD 都是JS的模块规范,但属于野生规范（开发者草拟并被广泛认可），ES6的 import/export 则是名门正统，由ECMAScript制定

# commonJS
  * commonJS API 提供了很多标准库，在其引用 module 里有自己的规范
  * require exports module
  * node 是参照 commonJS 规范实现的，因 node 发扬光大

# AMD
  * 浏览器不兼容 CommonJS 根本原因是  缺少四个环境变量 module, exports require global
  * 其二原因是 require是同步，会阻塞浏览器的渲染过程
  * AMD是"Asynchronous Module Definition"的缩写，意思就是"异步模块定义"。
  * AMD 异步回调的方式加载模块运行代码
    ```
    require(['math], function(math){
      math.add(2, 3)
    })
    ```
  * 目前 require.js curl.js 实现了AMD规范
  * data-main 指定程序入口，main.js 会被 require.js 加载
    ```
    <script src="js/require.js" data-main="js/main"></script>
    ```
  * 2014年式微
  
  * 由被 require.js 加载的模块必须 符合 AMD 规范

# CMD
  * 大名远扬的玉伯写了seajs，就是遵循他提出的CMD规范，与AMD蛮相近的
  * 导入模块写法
  ```
  define(['dep1', 'dep2'], function (dep1, dep2) {
      //Define the module value by returning a value.
      return function () {};
  });
  ```
  * 2014年式微

# AMD 和 CMD 差异探讨 及 懒加载 模块探讨
  * AMD 是将依赖的模块预加载好，CMD是等执行到依赖模块时才加载
  
# 懒加载 模块探讨

  * 一次性加载完是比动态加载在总体时间上是少的
  * 动态加载是在执行A语句去加载B模块导致错误时，数据回滚问题是否要考虑
  * 既然是引入了说明是要用的，为何要懒加载
  * JS 天生异步，却把模块懒执行，难以理解。

# ES6
  * 由于npm 上 CommonJS 库众多， Node 无法直接兼容 ES6，所以现阶段 require/exports 仍是必要的。目前 ES6语法是经过 babel 编译成 CommonJS来执行的。
  
# TS 简单学习

* TypeScript是JavaScript的超集
* TypeScript是结构性的类型系统
* 字面量类型
  ```
  let a: 'dog'; // 字符串字面量类型
  a = 'dog'; // 正确
  a = 'cat'; // error

  let c: 1|2|3; // 数字字面量类型。 ｜ 表示联合, c 可以是 1 或 2;
  c = 1; // 正确
  c = 3; // 正确, 
  c = 4; // 错误

  let d: true; // 布尔字面量类型
  ```

* <font color="red">typeof 生成一个联合字面量</font>

  ```
  const a = {
      a: 1,
      b: 2,
  };
  keyof typeof a; // 'a' | 'b'
  
  class A {
      c: number;
      d: number;
  }
  
  keyof A; // 'c' | 'd';


  function pluck<T, K extends keyof T>(o: T, names: Array<K>): T[K][] {
      return names.map(n => o[n]);
  }
  ```


## 变量
* 在构造函数的参数上使用public等同于创建了同名的成员变量。
  ```
  class Student {
    fullName: string;
    constructor(public firstName, public middleInitial, public lastName) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
  }
  ```

* 当你不知道类型时可以用 any, 相比下 <font color="red"> object 定义的变量只能赋值，不能访问。</font>


* 断言变量，表示对any类型的变量可能知道它有某个属性，ts无需检查，只在编译时检查

  ```
  类型断言有两种形式。 其一是“尖括号”语法：

  let someValue: any = "this is a string";

  let strLength: number = (<string>someValue).length;
  ```
  另一个为as语法 (JSX 仅支持as语法)：
  ```

  let someValue: any = "this is a string";

  let strLength: number = (someValue as string).length;
  ```

## 接口
  ```
  interface SquareConfig {
      color?: string; // 可选属性, 好处是可以帮忙检查有没有写错单词
      readonly width?: number; // 只读属性，无法赋值
      [propName: string]: any; // 字符串标签，允许接口还会带有任意数量的其它属性， 就是不用列很多无关属性
    }
  ```

  * 函数类型
  ```
  let mySearch: SearchFunc;
  mySearch = function(source: string, subString: string) {
    let result = source.search(subString);
    return result > -1;
  }
  ```
  <font color="red">不要求接受参数变量名相同</font>
  ```
  let mySearch: SearchFunc; 
  mySearch = function(src: string, sub: string): boolean {
    let result = src.search(sub);
    return result > -1;
  }
  ```

  * 可索引类型
  ```
  interface StringArray {
    [index: number]: string;
  }
  let myArray: StringArray;
  myArray = ["Bob", "Fred"];
  let myStr: string = myArray[0];
  ```

  * 类类型
  ```
  interface ClockInterface {
      currentTime: Date;
      setTime(d: Date);
  }

  class Clock implements ClockInterface {
      currentTime: Date;
      setTime(d: Date) {
          this.currentTime = d;
      }
      constructor(h: number, m: number) { }
  }
  ```


## 类
  * 在构造函数里访问 this的属性之前，我们 一定要调用 super()。<font color="red"> 这个是TypeScript强制执行的一条重要规则。</font>
  ```
  class Snake extends Animal {
    constructor(name: string) { super(name); }
  }
  ```

  * TypeScript里，成员都默认为 public, 可以标记 private、protected

  * 支持 get、set
  ```
  class Employee {
    private _fullName: string;
    set fullName(newName: string) {
      if (passcode && passcode == "secret passcode") {
          this._fullName = newName;
      }
      else {
          console.log("Error: Unauthorized update of employee!");
      }
    }
  }
  ```


  * 类可以被当作接口使用
    ```
    class Point {
      x: number;
      y: number;
    }

    interface Point3d extends Point {
        z: number;
    }
    ```

  ## 函数
    * 函数参数传入数量要一致, <font color="red">不同于JS</font>
    ```
    function buildName(firstName: string, lastName?: string) {
    return firstName + " " + lastName;
    }

    let result1 = buildName("Bob");                  // correct
    let result1 = buildName("Bob", "cat");           // correct
    let result2 = buildName("Bob", "Adams", "Sr.");  // error, too many parameters
    ```

    * 剩余参数的实现方式
    ```
    function buildName(firstName: string, ...restOfName: string[]) {
      return firstName + " " + restOfName.join(" ");
    }
    ```

  * 重载， 解决返回不同类型的问题
  它查找重载列表，尝试使用第一个重载定义。 如果匹配的话就使用这个。 因此，在定义重载的时候，一定要把最精确的定义放在最前面
  ```
  let suits = ["hearts", "spades", "clubs", "diamonds"];

  function pickCard(x: {suit: string; card: number; }[]): number;
  function pickCard(x: number): {suit: string; card: number; };
  function pickCard(x): any {
      // Check to see if we're working with an object/array
      // if so, they gave us the deck and we'll pick the card
      if (typeof x == "object") {
          let pickedCard = Math.floor(Math.random() * x.length);
          return pickedCard;
      }
      // Otherwise just let them pick the card
      else if (typeof x == "number") {
          let pickedSuit = Math.floor(x / 13);
          return { suit: suits[pickedSuit], card: x % 13 };
      }
  }

  let myDeck = [{ suit: "diamonds", card: 2 }, { suit: "spades", card: 10 }, { suit: "hearts", card: 4 }];
  let pickedCard1 = myDeck[pickCard(myDeck)];
  alert("card: " + pickedCard1.card + " of " + pickedCard1.suit);

  let pickedCard2 = pickCard(15);
  alert("card: " + pickedCard2.card + " of " + pickedCard2.suit);
  ```

  ## 泛型
    * 涉及`泛型变量、泛型接口、泛型类`
    * 使用方式及可以类型推断
    ```
    function identity<T>(arg: T): T {
        return arg;
    }
    let output = identity<string>("myString"); 
    let output = identity("myString");  //也可以被类型推断出，无需声明
    ```

    * 泛型约束
    ```
    // 表示T至少含有Lengthwise的属性。
    interface Lengthwise {
      length: number;
    }

    function loggingIdentity<T extends Lengthwise>(arg: T): T {
        console.log(arg.length);  // Now we know it has a .length property, so no more error
        return arg;
    }
    ```


    ## 枚举
      无

    ## 类型推断
      * 变量，推断发生在初始化变量和成员。
      ```
      let x = 2; // 会被推断为number类型。无需特别注明 let x: number = 2;
      ```

      * 数组，推断在所有可能类型里的最佳类型
      ```
      let x = [1, 2, 'string']; // 推断类型为 number[];
      ```

      * 上下文推断
      ```
      // 可推断出 onmousedown 方法里参数类型 mouseEvent
      window.onmousedown = function(mouseEvent) {
          console.log(mouseEvent.button);  //<- Error
      };
      ```

  ## 类型兼容
    * 结构化类型兼容
    <font color="red">TypeScript结构化类型系统的基本规则是，如果x要兼容y，那么y至少具有与x相同的属性</font>
    ```
    interface Named {
        name: string;
    }
    let x: Named;
    // y's inferred type is { name: string; location: string; }
    let y = { name: 'Alice', location: 'Seattle' };
    x = y;
    ```

    * 函数兼容
      要综合考虑 `参数、返回值`的类型+数量的问题

  ## 高级类型
    * 联合类型 
      * let a: number|string; // 可以是number，也可以是 string
      * function getSmallPet(): Fish | Bird {}; // 只能访问 Fish和Bird的<font color="red">共同</font>属性
    * 交叉类型 function(a: number): T & U{}; // 返回值既有T属性，也有U属性

    * 类型保护
      * is XX
      ```
      function isFish(pet: Fish | Bird): pet is Fish {
          return (<Fish>pet).swim !== undefined;
      }
      ```
      pet is Fish
      每当使用一些变量调用 isFish时，TypeScript会将变量缩减为那个具体的类型，只要这个类型与变量的原始类型是兼容的。
      ```
      // 'swim' 和 'fly' 调用都没有问题了

      if (isFish(pet)) {
          pet.swim();
      }
      else {
          pet.fly();
      }
      ```
      TypeScript不仅知道在 if分支里 pet是 Fish类型；<font color="red"> 它还清楚在 else分支里，一定 不是 Fish类型，一定是 Bird类型。</font>

      * typeof 可以不用专门写 isFish 判断函数。功能相同
      * instanceof

      * null
        类型检查器认为 null与 undefined可以赋值给任何类型。 <font color="red"> --strictNullChecks标记可以解决此错误</font>

      * 类型别名
        ```
        type Name = string;
        type NameResolver = () => string;
        type NameOrResolver = Name | NameResolver;
        function getName(n: NameOrResolver): Name {
            if (typeof n === 'string') {
                return n;
            }
            else {
                return n();
            }
        }
        ```
      * 字符串字面量类型
      ```
      type Easing = "ease-in" | "ease-out" | "ease-in-out";
      ```



  ## Symbols
    无

  ## 迭代器和生成器
    无

  ## 模块
    无

  ## 命名空间
    namespace, 可以多文件，当作一个文件来使用
    ```
    namespace Validation {
      export interface StringValidator {
          isAcceptable(s: string): boolean;
      }
    }
    ```

    


  ## JSX 无状态组件
    * 组件被定义成JavaScript函数，它的第一个参数是props对象
    ```
    interface FooProp {
        name: string;
        X: number;
        Y: number;
    }

    declare function AnotherComponent(prop: {name: string});
    function ComponentFoo(prop: FooProp) {
        return <AnotherComponent name={prop.name} />;
    }

    const Button = (prop: {value: string}, context: { color: string }) => <button>
    ```
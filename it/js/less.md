# less 基础语法
* https://less.bootcss.com/#

# scoped 的实现原理及穿透
* https://segmentfault.com/a/1190000015932467
 ![avatar](images/WX20200720-145633@2x.png)


## 功能 
  * 变量 可加减乘除
  * 混入。可复用
  * 嵌套
  * 内置函数
  * Map
```
@width: 10px;
// 变量加减。可像这样注释
@height: @width + 10px 
.border {
  width: @width;
  height: @height * 2;
} 

.set-con {
  .img-con {

  }
}

.img-con {
  // 混入
  .border();
  .set-con.img-con();
  .border[width];

  // 嵌套
  .img-text {
    color: #fff
  }
  &.img-border {
    // 内置函数 50%
    width: percentage(0.5)
  }
}

// 导入
@import "xx";
```
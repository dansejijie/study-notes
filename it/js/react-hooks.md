# hooks
  * http://www.ruanyifeng.com/blog/2019/09/react-hooks.html
  * React Hooks 的设计目的，就是加强版函数组件，完全不使用"类"，就能写出一个全功能的组件。

  * 所有钩子都是为了引入外部功能
    * useState() 
    * useContext()
    * useReducer()
    * useEffect()
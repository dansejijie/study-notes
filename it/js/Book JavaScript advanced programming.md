1. JavaScript 简介
  * JavaScript 的含义往往包含 <font color="red">ECMAScript，DOM（文档对象模型）、BOM(浏览器对象模型)</font>
  * DOM、BOM 往往由宿主环境提供，比如浏览器的 window


2. 在HTML中使用 JavaScript
  * <script></script> 由于不同浏览器对该属性的下载脚本等不同，因此适合放在页面底部，保证在所有浏览器里， <html>标签是最先开始解析

  * <noscript> 早期浏览器有些不支持 javascript,因此用noscript标签，保证在不支持的浏览器里，可以显示提示等信息


3. 基本概念
  * 函数内部可以放 'use strict'; 指定该函数执行严格模式
  * object 的实例都有下列属性
    * constructor
    * hasOwnProperty  在实例而非原型中。 in 可以判断在实例或原型中
    * isPrototypeOf
    * toString
    * valueOf 

  * 有符号右移 >>, <font color="red"> 无符号右移 >>> </font>

  * "23" < "3"; // true ,字符串匹配，不会先看字符长度

  * switch 支持任何数据类型

  * 函数的参数在内部中是用一个数组来表示的，在函数体内可以通过 arguments 来访问

  * arguments 赋值  
    ```
    function doAdd(num1, num2) {
      arguments[0] = 10;
      arguments[1] = 10;
      console.log(num1, num2)
    }

    doAdd(20); // 10, undefined
    ```

    arguments 里的数据跟 num1, num2 是独立的，但是对arguments赋值会导致num1同步，但若刚开始没传 num2,所以arguments[1] 与num2 无对应关系。所以即使赋值为 undefined


4. 变量、作用域和内存问题

  * <font color="red">对于引用类型的值</font>，我们可以添加属性和方法，可以改变和删除属性和方法

  * javaScript 没有块级作用域，var 声明的变量会被自动添加到最接近的环境中，入函数
  ```
  if(true) {
    var num1 = 1;
  }
  function add() {
    var num2 = 1;
  }

  console.log(num1, num2); // 1, undefined;
  ```

  * 垃圾收集
    * 大部分浏览器采用的是标记清楚，即变量进入环境，则标记，离开环境则清楚标记

    * 不常见的还有引用技术，该方法不好的地方是循环引用，因此要显示的设置为A = null;B=null

    * 在有的浏览器里，是可以触发垃圾收集过程的，但并不建议，在IE中,window,CollectGarbage()方法会立即执行垃圾收集



5. 引用类型
  * 创建 object实例的方法有两种
  ```
  var person = new Object();
  persion.name = 'Nicholas';

  var person1 = {
    name: 'Nicholas',
  }
  ```

  * Array 的创建有两种
  ```
  var colors = Array('a', 'b');

  var colors2 = ['a', 'b']
  ```

  * 队列方法
    unshift, shift 是向数组内的左边 和 右边增加数据

  * 数组迭代方法
    every() 每一个为true ，则为true
    some() 有一个为true， 则为true

  * 归并方法
    reduce 和  reduceRight


  * 函数名实际上也是一个指向函数对象的指针。


  * arguments.callee 指向了拥有这个arguments对象的函数。<font color="red">arguments.callee 在严格模式不可用</font>


  * prototype 属性是不可枚举的，因此不可用 for in 发现

  * apply() 和 call() 真正作用是扩充函数赖以运行的作用域

6. 面向对象的程序设计

  * 对象有属性类型
    Configurable， 是否可删除
    Enumerable 是否可枚举
    Writable 是否可写入
    Value

    ```
    Object.defineProperty(person, "name", {
      writable: false,
      value: "Nicholas"
    });
    ```

  * 访问器属性
    getter, setter

  * IE8中 Object.defineProperty 不支持


  * <font color="red">任何函数 可以通过 new 操作符来调用，那么它就是构造函数，如果不通过new 操作符调用，那么它跟普通函数没什么两样</font>


7. 函数表达式 

8. BOM

9. 客户端检测

10. DOM

11. DOM 扩展

12. DOM2 和 DOM3

13. 事件

14. 表单脚本

15. 使用 Canvas 绘图

16. HTML5脚本编程

17. 错误处理与调试

18. JavaScript 与 XML

19. E4X

20. JSON

21. Ajax 与 Comet

22. 高级技巧

23. 离线应用与客户端存储

24. 最佳实践

25. 新兴的API



# virtual-dom 算法
  * doc https://github.com/aooy/blog/issues/2
  * vue2.0 引入了 virtual dom
    doc https://github.com/vuejs/vue/blob/dev/src/core/vdom/patch.js
  
  * 一个节点会有超多属性，针对真实节点的操作会导致性能消耗巨大（如 遍历属性查看是否是同一个Dom）。
    ```
    var mydiv = document.createElement('div');
    for(var k in mydiv ){
      console.log(k)
    }
    ```

  * 
# 文档
  * doc
    https://babeljs.io/docs/en
  * github
    https://github.com/babel/babel



# babel 查看转义
  https://www.babeljs.cn/

# babel 能干什么
  * 语法转义 如es6转es5语法
  * Polyfill 支持
  * Source code transformations （不懂）

# xx
babel-polyfill
Babel默认只转换新的JavaScript句法（syntax），而不转换新的API，比如Iterator、Generator、Set、Maps、Proxy、Reflect、Symbol、Promise等全局对象，以及一些定义在全局对象上的方法（比如Object.assign）都不会转码。举例来说，ES6在Array对象上新增了Array.from方法。Babel就不会转码这个方法。如果想让这个方法运行，必须使用babel-polyfill，为当前环境提供一个垫片

# 文字理解
  * AST
  * Polyfill 填充工具，每个浏览器支持特性不同，可能有的不支持，就修补下。例如
    ```
      if(!Number.isNaN) {
        Number.isNaN = function(num) {
            return(num !== num);
        }
      }
    ```
  * @babel/polyfill
    由于 babel 假定是在 es5环境下运行代码，若环境不支持es5语法，如较低版本的IE，那么使用@ babel / polyfill将添加对这些方法的支持。
  * Babel cli 命令行工具

  * preset 预设，支持自动在node_module里查找对应module, 支持缩略文字

  * plugin 插件，支持自动在node_module里查找对应module, 支持缩略文字
    
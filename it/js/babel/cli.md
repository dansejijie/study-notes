# babel cli 
  babel 的命令行工具，支持 babel 的所有参数

# 利用babel-cli搭建支持ES6的node环境

  现在ES6盛行，开始大量使用ES6的特性敲代码，但限于Node.js本身对ES6的特性支持的不够完备，那么需要借助于其他工具来完成。
  基本上，现在都直接写ES6的代码，然后使用babel-cli提供的babel转换成ES5或者使用babel-node直接运行ES6的代码。

 ## 安装

  * 执行命令，全局安装babel-cli。
    ```
    npm install babel-cli -g
    ```

    babel-cli有两个主要的命令需要用到：

    babel：按照“.babelrc“文件转码js文件。

    babel-node：提供一个支持ES6的REPL环境，支持Node的REPL环境的所有功能，可以直接运行ES6代码。
 

## 直接运行ES6代码文件

  * 建一个工作文件夹，写一个如下简单代码的index-es6.js文件。
    ```
    let [a, b, c] = [1, 2, 3];
    console.log(a, b, c);
    ```

    运行命令执行index-es6.js文件。
    ```
    babel-node index-es6.js
    ```
    执行后可以看到结果。

 

## 将ES6转码成ES5代码文件

* 该种方法使用babel命令进行转码。

  babel命令需要使用“.babelrc“文件，其中会用到转码规则es2015，对应的模块是babel-preset-es2015，先要进行安装。

  在工作文件夹中执行命令安装。
  ```
  npm install babel-preset-es2015 --save-dev
  ```
  安装完成后，在工作文件夹中创建文件”.babelrc“，内容为。
  ```

  {
      "presets": [
          "es2015"
      ],
      "plugins": []
  }
  ```
  执行以下命令进行ES6到ES5的转码。
  ```
  babel index-es6.js -o index-es5.js
  ```
  可以看到转码后的idnex-es5.js的内容，很方便。

  复制代码
  ```
  "use strict";

  var a = 1,
      b = 2,
      c = 3;

  console.log(a, b, c);
  ```
# 参考文档
  * 理解 http://www.ruanyifeng.com/blog/2013/01/javascript_source_map.html
  * 练习 https://blog.fundebug.com/2017/03/13/sourcemap-tutorial/

# source map 缘由
  js 源码大多经过转换才投入生产，源码转换主要涉及三种情况：
  * 压缩，减小体积。比如jQuery 1.9的源码，压缩前是252KB，压缩后是32KB。
  * 多个文件合并，减少HTTP请求数。
  * 其他语言编译成JavaScript。最常见的例子就是CoffeeScript。

  代码经过转换后，找错就会变得困难重重，Source map就是一个信息文件，里面储存着位置信息。也就是说，转换后的代码的每一个位置，所对应的转换前的位置。

# 样例
   转换后源码（看最后一行， 引入 source map）： http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js

   map 文件信息
   http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.map

# 工具
  很多打包工具都支持 source map 文件输出，如 UglifyJS2


# 浏览器如何启用 source map
  要在转换后的代码尾部，加上一行就可以了。
  ```
  　//@ sourceMappingURL=/path/to/file.js.map
  ```

# 如何生成 source map
...

# source map 格式
  打开Source map文件，它大概是这个样子：
  ```
  　　{
　　　　version : 3,
　　　　file: "out.js",
　　　　sourceRoot : "",
　　　　sources: ["foo.js", "bar.js"],
　　　　names: ["src", "maps", "are", "fun"],
　　　　mappings: "AAgBC,SAAQ,CAAEA"
　　  }
  ```


  整个文件就是一个JavaScript对象，可以被解释器读取。它主要有以下几个属性：

 
　　- version：Source map的版本，目前为3。

　　- file：转换后的文件名。

　　- sourceRoot：转换前的文件所在的目录。如果与转换前的文件在同一目录，该项为空。

　　- sources：转换前的文件。该项是一个数组，表示可能存在多个文件合并。

　　- names：转换前的所有变量名和属性名。

　　- mappings：记录位置信息的字符串，下文详细介绍。


  * mappings属性

  下面才是真正有趣的部分：两个文件的各个位置是如何一一对应的。

  关键就是map文件的mappings属性。这是一个很长的字符串，它分成三层。

  　　第一层是行对应，以分号（;）表示，每个分号对应转换后源码的一行。所以，第一个分号前的内容，就对应源码的第一行，以此类推。

  　　第二层是位置对应，以逗号（,）表示，每个逗号对应转换后源码的一个位置。所以，第一个逗号前的内容，就对应该行源码的第一个位置，以此类推。

  　　第三层是位置转换，以VLQ编码表示，代表该位置对应的转换前的源码位置。

  举例来说，假定mappings属性的内容如下：

  　　mappings:"AAAAA,BBBBB;CCCCC"


  
# Blob File Base64

## Blob
  * Binary Large Object的缩写，代表<font color="red">二进制类型的大对象</font>
  * File 继承 Blob
  * Blob 具有slice方法，因此应用场景可以用在 <font color="red">分片上传文件</font>
  * const blob = new Blob([data]);

## Blob URL
  * 格式：blob:http://xxx
  * const blobUrl = URL.createObjectURL(blob);
  * 应用场景: <font color="red">文件下载地址、图片资源地址</font>
  * <font color="red">URL.revokeObjectURL(blobUrl)</font>, 释放资源

  ![avatar](images/WX20200707-115734@2x.png)

## DATA URL
  * 直接存储base64编码(如图片)后得数据，往往很长,适合显示加载<font color="red">小</font>图片。大图片适合用 blob url。
  * Blob URL复制到浏览器的地址栏中，是无法获取数据的。Data URL相比之下，就有很好的移植性，你可以在任意浏览器使用
  * 格式: "data:image/gif;base64,ROlXXXXXXXX"


## 相互转换

* base64 => blob

```js
export const b64toBlob = (b64Data, contentType, sliceSize) => {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, { type: contentType });
  return blob;
};
```

* file => base64
```js
var reader = new FileReader();
reader.readAsDataURL(this.files[0]);
reader.onload = function(){
    console.log(reader.result); //获取到base64格式图片
};
```

* base64 => dataURL
  ```
  const b64Data = "ROLXXXXXXXX";
  const dataUrl = `data:image/gif;base64,${b64Data}`
  ```

* dataURL => File
  ```
  dataURLtoFile(dataurl, filename) { 
	    var arr = dataurl.split(','),
	        mime = arr[0].match(/:(.*?);/)[1],
	        bstr = atob(arr[1]),
	        n = bstr.length,
	        u8arr = new Uint8Array(n);
	    while (n--) {
	        u8arr[n] = bstr.charCodeAt(n);
	    }
	    return new File([u8arr], filename, { type: mime });
	}
  ```

* dataURL => blob => File 
  * 避开IOS没有 new File() 问题
  ```
  //将base64转换为blob
  dataURLtoBlob: function(dataurl) { 
      var arr = dataurl.split(','),
          mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[1]),
          n = bstr.length,
          u8arr = new Uint8Array(n);
      while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
      }
      return new Blob([u8arr], { type: mime });
  },
  //将blob转换为file
  blobToFile: function(theBlob, fileName){
      theBlob.lastModifiedDate = new Date();
      theBlob.name = fileName;
      return theBlob;
  },
  //调用
  var blob = dataURLtoBlob(base64Data);
  var file = blobToFile(blob, imgName);
  ```

* url => dataUrl
  ```
  function getBase64(url, callback) {
    //通过构造函数来创建的 img 实例，在赋予 src 值后就会立刻下载图片，相比 createElement() 创建 <img> 省去了 append()，也就避免了文档冗余和污染
    var Img = new Image()
    var dataURL = ''
    Img.setAttribute('crossorigin', 'anonymous') // 注意设置图片跨域应该在图片加载之前 (本地图片则不需要此设置)
    Img.src = url
    Img.onload = () => {
        //要先确保图片完整获取到，这是个异步事件
        var canvas = document.createElement("canvas") //创建canvas元素
        var width = Img.width //确保canvas的尺寸和图片一样
        var height = Img.height
        canvas.width = width
        canvas.height = height
        canvas.getContext("2d").drawImage(Img, 0, 0, width, height) //将图片绘制到canvas中
        dataURL = canvas.toDataURL("image/jpeg", 1) //转换图片为dataURL 图片质量0~1默认值0.92
        callback ? callback(dataURL) : null; //调用回调函数
    }
  }
  ```


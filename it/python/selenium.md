# selenium
* doc 
  https://selenium-python.readthedocs.io/getting-started.html

* 介绍
  * Selenium 是用于测试Web应用程序用户界面<font color="red">UI</font>的常用测试框架。

* selenium 由三种工具组成
  * SeleniumIDE，是Firefox的扩展插件，<font color="red">支持用户录制和回访测试。</font>录制/回访模式存在局限性，对许多用户来说并不适合。

  * Selenium WebDriver提供了各种语言环境的API来支持更多控制权和编写符合标准软件开发实践的应用程序。

  * SeleniumGrid帮助工程师使用Selenium API控制分布在一系列机器上的浏览器实例，支持并发运行更多测试。

* 历史
  * Selenium1 基于 Jsvascript 的自动化引擎，不足之处是浏览器对JS有很多安全限制，限制测试
  * WebDriver 基于 Selenium1 开发，解决Javascript环境沙箱问题
  * Selenium2 是 Selenium1和 WebDriver的合并后产生的项目
  * 2016年 Selenium3 移除了 Selenium RC，并重写了所有浏览器的驱动。

  * Selenium RC 是支持多种不同的语言编写自动化测试脚本，通过selenium RC 的服务器<font color="red">作为代理服务器</font>去访问应用从而达到测试的目的。

# WebDriver
  * WebDriver提供了各种语言环境的API来支持更多控制权和编写符合标准软件开发实践的应用程序。

  * <font color="red">得到浏览器厂商的支持，WebDriver还利用操作系统级的调用模拟用户输入。</font>

  * 实现使用了经典的Client-Server模式。客户端发送一个requset（WebDriver），服务器端（浏览器）返回一个response。

  * 运行浏览器的机器。Firefox浏览器直接实现了WebDriver的通讯协议，而Chrome和IE则是通过ChromeDriver和InternetExplorerDriver实现的。


# 同类比较
  * selenium 类似一个驱动，驱动真实浏览器访问。
  * PhantomJS	，是一个基于 <font color="red">WebKit</font>（WebKit是一个开源的浏览器引擎，Chrome，Safari就是用的这个浏览器引擎） 的服务器端JavaScript API，主要应用场景是：无需浏览器的 Web 测试，页面访问自动化，屏幕捕获，网络监控。<font color="red">不足点是涉及页面有很多UI操作的化不适合</font>

  * PhantomJS 的使用
    ```    
    from selenium import webdriver // 导入 webdriver 对象
   
    driver = webdriver.PhantomJS()  // 初始化 PhantomJS 浏览器
    
    driver.get('http://www.baidu.com/') // 访问百度

    driver.find_element_by_id("wrapper").text // # 获取 id 为 wrapper 这个元素的 text
    ```


# 实践 （mac上使用selenium自动运行chrome）
  https://www.cnblogs.com/luozx207/p/8513526.html


  * 用我们的老朋友pip把selenium装好=
    ```
    pip install selenium

    ```
　　

  * 用webdriver.Chrome启动Chrome浏览器
    ```
    from selenium import webdriver
    if __name__ == "__main__":
      driver = webdriver.Chrome()
      driver.get('http://www.baidu.com')
    ```
　　

    但是报错：WebDriverException: 'chromedriver' executable needs to be in PATH

查了一下，是因为mac的/usr/bin/中没有chromedriver这个驱动

  * 下载对应版本的chromedriver

    chromedriver下载地址：

    http://npm.taobao.org/mirrors/chromedriver/

    注意 ：chromedriver的版本要与你使用的chrome版本对应


 

  * 将Chromedriver放到/usr/bin/路径

    mac系统在10.11版本之后就不能修改usr、bin等系统文件夹的内容了。要开启权限需要进入保护模式：

    1、重启，重启过程中按option键

    2、在一个磁盘页面弹出来之后，按command+R

    3、会出现一个苹果图标并加载很久，不要害怕，这不是在重装系统

    4、在保护模式界面从左上角打开终端，输入

    1
    csrutil disable
    5、重启，command+C复制Chromedriver，在finder中通过‘前往-->前往文件夹-->输入/usr/’进入usr隐藏文件夹，然后进入bin，command+V，再输入一次用户密码，就可以把Chromedriver复制到/usr/bin/了（之所以这么麻烦是因为在终端用cp指令复制还是显示没有权限）

 

  * 再运行一次之前的python程序，成功打开Chrome






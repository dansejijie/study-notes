# flutter
  * doc https://flutterchina.club/
  * 跨平台APP开发，相比RN的一次学习到处写（兼容不同平台），flutter 是一次学习一次写。
  * 使用Dart语言，谷歌开发的语言。


## 跨平台解决方案原理
  * flutter 用自己的渲染引擎渲染组件。不涉及原生的。
  * Flutter将UI组件和渲染器从平台移动到应用程序中，这使得它们可以自定义和可扩展。Flutter唯一要求系统提供的是canvas，以便定制的UI组件可以出现在设备的屏幕上。

## 生态

  * 基本常见的库都有，但是目前还处于 beta阶段，未到release阶段。
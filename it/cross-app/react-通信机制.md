# react-通信机制
  * doc https://www.cnblogs.com/android-blogs/p/5623481.html

## 总结
  * java 和 js 通信是靠 配置表来通信的
  * 每个config都存有组件或module的信息，通过<font color="red">id</font>来查询，并通过反射的方法执行对应方法
  ![avatar](images/894rwehfiw03812938jhefi90342.png)


## config 生成的步骤
  * MainActivity 继承 ReactActivity, 实现 onCreate 方法
  * onCreate -> 创建 <font color="red">ReactRootView</font>,并实现对应的 startReactApplication 方法 （很多与JS交互的代码都在这里实现）。
  * 该方法里创建 <font color="gree">ReactInstanceManager</font> 类， 并调用该类的一个方法开启 后台创建 <font color="red">ReactApplicationContext</font> 的 React上下文的线程。中间还涉及了根据是否是开发模式选择不同地方的JSBundle

  * 在创建 ReactApplicationContext 的方法里，主要涉及三个 module。
    * 创建 NativeModuleRegistry，
    * 创建 JSIModule
    * 创建 <font color="red">CatalystInstanceImpl</font>，核心module，包裹上面两个module<font color="gree">（拥有两个配置表）</font> ，并创建 ReactBridge 对象，用该对象实现<font color="red">JNI</font>技术以此实现JAVA和JS的交互。（用ReactBridge将java的注册表通过jni传输到了JS层，实现JS层拥有java层的所有接口和方法）

    * 当一切创建好后，会调用ReactRootView里的一个方法，该方法会先调用catalystInstance 的 .getJSModule(AppRegistry.class)方法。 它会去调用JavaScriptModuleRegistry的getJSModule方法，获取对应的JavaScriptModule，也就是从注册表中获取对应的模块。
    * 找到module后，然后调用
    <font color="red">runApplication </font> 方法，将想用调用的方法对应的<font color="red">moduleId</font> 和arguments通过jni传递到js端进行调用。从而实现JS层的渲染。

      ![avatar](images/83jsadkshd0930923asdh.png)


## JS 调用 Java
  * JS将方法的对应参数push到MessageQueue中，等java端事件传递

  * Java端事件触发以后，JS层将MessageQueue中的数据通过C层传递给java层

  * C层调用一开始注册在其中的NativeModulesReactCallback

  * 然后通过JavaRegistry这个Java注册表拿到对应的module和method

  * 通过反射执行方法。

  ![avatar](images/389jdia2039jsddskd.png)
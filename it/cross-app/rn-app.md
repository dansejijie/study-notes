做一个 rn-app 需要预备的工作
===================================

需求
-----------------------------------

前后端公共库 （数据交互协议）
-----------------------------------


前端
-----------------------------------
## 辅助工具

  * 编辑器 （vscode）
    1. 插件( 代码拼写 spell, ) 

## 设计
  1. 约定调色板

      textPrimary: '#f44336', // 文本主色
      textSecondary: '#8E9296', // 文本副色
      textHint: '#bbb', // 文本输入提示色
      
      secondary: '#8ECF93', // 强调色
      error: '#f44336', // 错误色
      warning: '#ffc600', // 警告色

      backgroundBase: '#fff', // 默认色
      backgroundPage: '#f6f6f7', // 页面背景色

      divider: '#e7e7ea', // 分割线色
      borderColor: '#DCE0E6', // 边框色

  2. 约定字体大小

      textSmall: 12,
      textNormal: 14,
      fontLarge: 16,


  3. 约定字体类型

      title: {
        fontSize: 14,
        color: '#f44336',
      }, // 标题
      desc: {
        fontSize: 14,
        color: '#8E9296',
      }, // 副文本

  4. 约定宽度大小

      dPage: 12, // 组件与页面的距离
      dBlank: 10, // 分割组件距离
      dContainer: 12, // 组件与父组件距离
      dSimpleLine: 44, // 单行列表项高度 

  5. 约定父组件容器布局
      cFlex: {
        flex: 1,
        padding: 12,
        backgroundColor: '#fff',
      },
      cC: {
        padding: 12,
        backgroundColor: '#fff',
      }, // 通用布局
      cCCenter: {
        padding: 12,
        ju
      },
      cRow: {
        flexDirection: 'row',
        padding: 12,
        backgroundColor: '#fff',
      }, // 横向布局
      cRowCenterBetween: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 12,
        backgroundColor: '#fff',
      },


## 组件库
  * PureComponent
  * Header
  * Touchable
  * Page 自动加载
  * ListPage 自动加载下一页
  * Dialog、ActionSheet、Pick
  * Divider
  * SearchBar
  * ArrowRight
  * Toast
  * Prefernce // 表单

## 工具库

  1. 网络请求 （axios）（每次请求携带用户信息）
  2. APP更新 （热更新 react-native-code-push 、原生更新 bugly）
  3. 图片上传 （七牛）
  4. 图片选择
  5. 数据流管理 （dva）
  6. 数据持久化管理 (Storage)





后端
-----------------------------------

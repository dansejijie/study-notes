# 组件实现方式
  * React.createClass 试用API来定义组件
  * class component 用 ES6 的class 来定义组件 （适合用到生命周期、state的时候用）
  * Functional stateless component 通过函数定于无状态组件 （适合 无生命周期、state的时候用）

# 受控组件 和 非受控组件
  * 受控组件是指由外部传值改变组件属性，如  TextInput 的 value
  * 非受控组件是指组件内部自己维护一套state, 想要改变值的花需要拿到 ref 值
  * 谷歌推荐 使用 受控组件。

# 高阶组件
  * https://segmentfault.com/a/1190000010371752
  * 高阶组件 是通过包括被传入的React组件，经过一系列处理，返回一个相对增强的React组件，供其他组件调用, 本质上是一个函数去包裹组件，是<font color="red" >装饰器</font>在React的应用
    ```
      export default function withHeader(WrappedComponent) {
        return class HOC extends Component {
          render() {
            return <div>
              <div className="demo-header">
                我是标题
              </div>
                <WrappedComponent {...this.props}/>
            </div>
          }
        }
      }
    ```
    <font color="red" >反向继承</font> 相比 extend 更灵活, 因为未制定 父类

  ```
      export default function (WrappedComponent) {
        return class Inheritance extends WrappedComponent {
          componentDidMount() {
            // 可以方便地得到state，做一些更深入的修改。
            console.log(this.state);
          }
          render() {
            return super.render();
          }
        }
      }
  ```

# state

## 原理

  * 原理
    * Batch Upate (批量更新) \
      即实现一个 queue，setState后 入队列，在合适的时候 flush
      React 用 Transaction 来实现，即在要执行的函数中用事物包裹起来，在函数执行前加入 initialize （生成一个 queue）, 在函数执行后 加入 close 阶段 （fluse queue）

      以下由于 setTimeout 不受 React 控制，会导致 render 两次。
      ```
      setTimeout(()=>{
        this.setState({foo: 1});
        this.setState({foo: 2})
      })
      ```

## setState 

  * 同步更新策略 （允许第一个参数是状态计算函数）

      ```
      this.setState((prevState, props) => ({
        count: prevState.count + 1
      }));

      this.setState((prevState, props) => ({
          count: prevState.count + 1
        }));
      ```

  * 异步更新 回调

      ```
      this.setState(
        { username: 'tylermcginnis33' },
        () => console.log('我已经更新了')
      )
      ```

## react-dom-diff
  * doc https://www.infoq.cn/article/react-dom-diff
  * web 页面由DOM树构成(DOM 操作节点的接口)
  * 标准的DOM算法：任意给定两颗树，找到最少的转换步骤，时间复杂度 <font color="red" >O(n^3)</font> \

    树之间比较->同一层同一位置节点比较 ->(1、节点类型，2、节点属性)
      ![avatar](images/1f522dc11891365ce77c7650f517495a.png)

    * 节点类型不同：<font color="red" >销毁</font>->重建 (ABCDE 与 ABFCDE)
      ![avatar](images/eb7b537c6ab5ac31ffcc6483fb7fba86.png)
      ![avatar](images/c870788f72025a49a6781c5135df38f9.png)
    
    * 相同类型节点比较: 删除不一样属性->增加多的属性

  * react的虚拟DOM算法 <font color="red" >O(n)</font> \
    * 对于同一层次的一组子节点，它们可以通过唯一的 <font color="red" >id</font> 进行区分。
    * 两个相同组件产生类似的 DOM 结构，不同的组件产生不同的 DOM 结构；

    * 针对列表节点优化 (列表页，子item, id) \
      每个节点唯一的标识（key），那么 React 能够找到正确的位置去插入新的节点 \
      ![avatar](images/ffe3529b4043ea4b37be8563ccb1fe6f.png)


## JSX （为什么用JSX）
  * doc https://www.infoq.cn/article/react-jsx-and-component
  * JSX 其实就是一个语法糖，在打包的时候转换为JS代码了的。
  * 在现在框架都流行UI和业务代码分离的今天，为什么 React 要引入JSX。
    * 直观的看页面布局。
    * 模板的复杂性导致框架需要精心设计，导致臃肿、难以调试
      * 模板要对应数据模型、即上下文、如何去绑定和实现
      * 模板可以嵌套，不同部分界面可能来自不同数据模型，如何处理？
      * 模板语言终究是一个轻量级语言，为了满足项目需求，你很可能需要扩展模板引擎的功能。
  * JSX 编译器 JSTransForm 在2015年不再维护，全部采用第三方Babel的 JSX 编译器实现，拥抱开源。


# 零碎知识
  * react-native 导出的ios包安装在平板涉及兼容问题，ipad显示的app两边有黑屏问题。可以在xcode里设置app打包的目标设备为 ipad 即可。
    https://stackoverflow.com/questions/31175697/react-native-doesnt-fill-up-window-on-ipad
    https://blog.csdn.net/quantum7/article/details/80983168

# 相关库

## react-lifecycles-compat

  * 因16.3版本后废弃 `componentWillMount`, `componentWillReceiveProps`, and `componentWillUpdate`. 新增 `getDerivedStateFromProps` 和`getSnapshotBeforeUpdate`

  * `getDerivedStateFromProps` 用于替代 componentWillReceiveProps, 返回null表示新的props不需要任何state更新。

  * `getSnapshotBeforeUpdate` 用于替代 componentWillUpdate, 其的返回值将作为第三个参数传递给componentDidUpdate。

  * react-lifecycles-compat 是为了在16.3之前版本就可以`getDerivedStateFromProps` 和`getSnapshotBeforeUpdate`用



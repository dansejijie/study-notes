# 踩坑日记

* zIndex 样式
  禁止用zIndex 样式，会出现各种布局遮挡问题，比如 unity。
* Android Image 不能正确显示大长图
    若是一个 375*3700的图，Image.getSize 得到宽高可能是90*900，然后显示在很大的容器里会显示很模糊
    目前解决办法： 引入 react-native-fast-image 内部引用的是glide

* Android ScrollView style 设置宽高 无效
    给ScrollView设置宽高无效，即使设置 flex: undefined. 原因是ScrollView的 contentContainerStyle 默认有 flexGrow: 1属性,因此要给 flexGrow: undefined 属性。


* ref 与 componentDidMount 调用时机

    <font color="red">1、2、3、4、5</font>
    ```
    class Demo extends Component {
        constructor(props) {
            super(props);
            this.state = {
                show: false,
            }
        }
        componentDidMount() {
            console.log('5 ref', 'componentDidMount')
            setTimeout(()=>{
                this.setState({
                    show: true,
                })
            }, 2000)
        }
        render() {
            const { show } = this.state;
            return (
                <View style={styles.container}>
                    <View ref={(Ref)=>{console.log('1 ref', 'create', Ref)}}></View>
                    {show &&<RefView ref={(Ref)=>{console.log('4 ref', 'out create', Ref)}}></RefView>}
                </View>
            )
        }
    }

    class RefView extends Component {
        componentDidMount() {
            console.log('3 ref', 'componentDidMount')
        }
        render() {
            return <View ref={(Ref)=>{console.log('2 ref', 'create', Ref)}}></View>
        }
    }
    ```

* react-navigation navigate运行机制

  naviagte 就是若出现 Stack里未出现的页面，则往上堆叠，若有历史的页面，则直接路由到历史页面，并把该页面上面的所有页面都卸载掉
